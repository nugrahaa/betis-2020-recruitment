from django.shortcuts import render

def ekonomi(request):
    context = {
        'judul' : 'Ekonomi',
    }

    return render(request, 'ekonomi/ekonomi.html', context)
