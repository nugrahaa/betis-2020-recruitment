from django.shortcuts import render

def fisika(request):
    context = {
        'judul' : 'Fisika',
    }

    return render(request, 'fisika/fisika.html', context)