### Description / Overview
# This is a website for BETIS 2020 Recruitment 

### Requirements 
general requirement :
- Python
- Django
- Git
others : check requirements.txt

### Installation / Build Instruction
to reuse this project do:
```
git clone https://gitlab.com/nugrahaa/betis-2020-recruitment.git
```
go to the new project directory
```
pip install -r requirements.txt
```
and then customize it all you want.

### How To Run
- to run this in local. after you build the project do :
```
python manage.py runserver
```
- to run this in server do deploy the way you prefer.

### Author
- Ari Angga Nugraha

