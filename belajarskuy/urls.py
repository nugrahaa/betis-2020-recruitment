from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('fisika/', include('fisika.urls')),
    path('ekonomi/', include('ekonomi.urls')),
    path('biologi/', include('biologi.urls')),
]
