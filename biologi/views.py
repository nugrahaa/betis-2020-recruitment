from django.shortcuts import render

def biologi(request):
    context = {
        'judul' : 'Biologi',
    }

    return render(request, 'biologi/biologi.html', context)
