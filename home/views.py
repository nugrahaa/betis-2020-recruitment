from django.shortcuts import render

def home(request):
    context = {
        'judul' : 'Home',
    }

    return render(request, 'home/home.html', context)